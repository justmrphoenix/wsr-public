﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AuthForm : Form
    {
        int failedAttemps = 0;
        public AuthForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Helper.GetConnection().State != System.Data.ConnectionState.Open)
            {
                MessageBox.Show("Ошибка подключения к БД!\nПовторите попытку позже!", "Ошибка");
                return;
            }
            MySqlCommand command = Helper.CreateCommand();
            string password = textBox2.Text;
            password = Helper.GetHashString(password);
            command.CommandText = "SELECT * FROM users WHERE login='" + textBox1.Text + "' AND password='" + password + "';";
            MySqlDataReader reader = command.ExecuteReader();
            using (reader)
            {
                if (reader.Read())
                {
                    string role = reader.GetString("role");
                    Helper.userRole = role;
                    (new MainForm()).Show();
                    this.Hide();
                    MessageBox.Show("Добро пожаловать, " + role);
                }
                else
                {
                    failedAttemps++;
                    if(failedAttemps == 3)
                    {
                        button1.Enabled = false;
                        Timer timeoutTimer = new Timer();
                        timeoutTimer.Interval = 3 * 60 * 1000;
                        timeoutTimer.Tick += delegate {
                            failedAttemps = 0;
                            button1.Enabled = true;
                            timeoutTimer.Stop();
                            timeoutTimer.Dispose();
                        };
                        timeoutTimer.Start();
                        MessageBox.Show("Вы ввели неверный логин или пароль больше трёх раз, система заблокирована!", "Блокировка");
                    }
                    else
                    {
                        MessageBox.Show("Вы ввели неверный логин или пароль!", "Ошибка");
                    }
                    return;
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
            this.Hide();
        }

        private void AuthForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
