﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


public class Helper
{
    private static MySqlConnection mySqlConnection;
    public static string userRole;
    public static byte[] GetHash(string inputString)
    {
        HashAlgorithm algorithm = SHA256.Create();  //or use MS5.Create();
        return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
    }

    public static string GetHashString(string inputString)
    {
        StringBuilder sb = new StringBuilder();
        foreach (byte b in GetHash(inputString))
            sb.Append(b.ToString("X2"));

        return sb.ToString();
    }
        
    public static MySqlConnection GetConnection()
    {
        if (Helper.mySqlConnection == null)
        {
            Helper.mySqlConnection = new MySqlConnection();
            Helper.mySqlConnection.ConnectionString = "Server=localhost;UserId=root;Password=toor;Database=wsr";
            try
            {
                Helper.mySqlConnection.Open();
            } catch {}
        }
        return Helper.mySqlConnection;
    }

    public static MySqlCommand CreateCommand()
    {
        return Helper.GetConnection().CreateCommand();
    }
}

