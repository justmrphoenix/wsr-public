﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MySqlCommand registerCommand = Helper.CreateCommand();
            registerCommand.CommandText = "INSERT INTO users (`login`, `password`, `role`) VALUES('"+textBox1.Text+"','"+Helper.GetHashString(textBox1.Text)+"','"+comboBox1.Text+"')";
            try
            {
                registerCommand.ExecuteNonQuery();
            } catch
            {
                // Если получили ошибку, то, скорее всего, такой пользователь уже сущетсвует, выводим ошибку
                MessageBox.Show("Такой пользователь уже существует", "Ошибка");
                // Ранний выход чтобы предотвратить последующее выполнение
                return;
            }
            MessageBox.Show("Создан новый пользователь с id " + registerCommand.LastInsertedId, "Успех");
        }

        private void RegisterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            (new AuthForm()).Show();
        }
    }
}
