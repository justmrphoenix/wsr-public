﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class HelperTests {
        [TestMethod()]
        public void GetHashStringTest() {
            string testString1 = "test";
            string teshHash1 = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08".ToUpper();
            string actualResult1 = Helper.GetHashString(testString1);
            Assert.AreEqual(teshHash1, actualResult1);

            string testString2 = "test2";
            string teshHash2 = "60303ae22b998861bce3b28f33eec1be758a213c86c93c076dbe9f558c11c752".ToUpper();
            string actualResult2 = Helper.GetHashString(testString2);
            Assert.AreEqual(teshHash2, actualResult2);

            string testString3 = "abcdefghijklmnopqrstuvwxyz";
            string teshHash3 = "71c480df93d6ae2f1efad1447c66c9525e316218cf51fc8d9ed832f2daf18b73".ToUpper();
            string actualResult3 = Helper.GetHashString(testString3);
            Assert.AreEqual(teshHash3, actualResult3);
        }

        [TestMethod()]
        [Timeout(5000)]
        public void TestDatabaseConnection() {
            MySqlConnection connection = Helper.GetConnection();
            Assert.AreEqual(connection.State, System.Data.ConnectionState.Open);
        }
    }

    [TestClass()]
    public class UserManagment{
        [TestMethod()]
        public void TestUserAuthorization() {
            string login1 = "test";
            string password1 = "test";
            string passwordHash1 = Helper.GetHashString(password1);
            MySqlCommand command1 = Helper.CreateCommand();
            command1.CommandText = "SELECT * FROM users WHERE login='" + login1 + "' AND password='" + passwordHash1 + "';";
            MySqlDataReader reader1 = command1.ExecuteReader();
            using (reader1) {
                Assert.IsTrue(reader1.Read());
                Assert.AreEqual(reader1.FieldCount, 4);
                Assert.AreEqual(reader1.GetString("role"), "role1");
            }

            string login2 = "test2";
            string password2 = "test2";
            string passwordHash2 = Helper.GetHashString(password2);
            MySqlCommand command2 = Helper.CreateCommand();
            command2.CommandText = "SELECT * FROM users WHERE login='" + login2 + "' AND password='" + passwordHash2 + "';";
            MySqlDataReader reader2 = command2.ExecuteReader();
            using (reader2) {
                Assert.IsTrue(reader2.Read());
                Assert.AreEqual(reader2.FieldCount, 4);
                Assert.AreEqual(reader2.GetString("role"), "role2");
            }
        }

        [TestMethod()]
        public void TestAuthorizationWithIncorrectUser() {
            string login1 = "NO_USER_SRSLY";
            string password1 = "U_CALL_THAT_A_PASSWORD??";
            string passwordHash1 = Helper.GetHashString(password1);
            MySqlCommand command1 = Helper.CreateCommand();
            command1.CommandText = "SELECT * FROM users WHERE login='" + login1 + "' AND password='" + passwordHash1 + "';";
            MySqlDataReader reader1 = command1.ExecuteReader();
            using (reader1) {
                Assert.IsFalse(reader1.Read());
            }
        }

        [TestMethod()]
        public void AddUserTest() {
            MySqlCommand command = Helper.CreateCommand();
            command.CommandText = "INSERT INTO users (`login`, `password`, `role`) VALUES('testUser','FD5CB51BAFD60F6FDBEDDE6E62C473DA6F247DB271633E15919BAB78A02EE9EB','testRole')";
            command.ExecuteNonQuery();
        }

        [TestMethod()]
        public void DeleteUserTest() {
            MySqlCommand command = Helper.CreateCommand();
            command.CommandText = "DELETE FROM `wsr`.`users` WHERE (`login` = 'testUser');";
            command.ExecuteNonQuery();
        }
    }
}